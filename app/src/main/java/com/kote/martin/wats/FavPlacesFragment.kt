package com.kote.martin.wats

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.kote.martin.wats.model.Place
import com.kote.martin.wats.model.User
import java.util.*
import com.kote.martin.wats.persistence.AppDatabase


/**
 * A fragment representing a list of Items.
 * Activities containing this fragment MUST implement the
 * [FavPlacesFragment.OnListFragmentInteractionListener] interface.
 */
class FavPlacesFragment : Fragment() {

    private var columnCount = 1

    var placesAdapter: PlacesRecyclerViewAdapter? = null
    private var listener: OnListFragmentInteractionListener? = null
    var userId: Long? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
            userId = it.getLong(USER_ID)
        }

        placesAdapter = PlacesRecyclerViewAdapter(listener)

        val user = User(1, "Kosta", "adfsdf", "koki", "")

        val places: List<Place> = Arrays.asList(
                Place(1, "Skopje", "ChIJpaKajKUVVBMRICCHYMKI7bI", 1),
                Place(2, "Veles", "ChIJNcW50sErVBMRNeXeKz-ZzeQ", 1)
        )


//        val db = AppDatabase.getAppDatabase(context!!)
//        db.userRepo().insert(user)
//        db.placeRepo().insertAll(places)
//        val x = db.userRepo().findUserWithFavPlaces(1).places
        placesAdapter?.setData(places)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_fav_places_list, container, false)

        // Set the adapter
        if (view is RecyclerView) {
            with(view) {
                layoutManager = when {
                    columnCount <= 1 -> LinearLayoutManager(context)
                    else -> GridLayoutManager(context, columnCount)
                }
                adapter = placesAdapter
            }
        }
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnListFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson
     * [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnListFragmentInteractionListener {
        fun onListFragmentInteraction(item: Place?)
    }

    companion object {

        const val USER_ID = "userId"
        const val ARG_COLUMN_COUNT = "column-count"

        @JvmStatic
        fun newInstance(columnCount: Int) =
                FavPlacesFragment().apply {
                    arguments = Bundle().apply {
                        putInt(ARG_COLUMN_COUNT, columnCount)
                        putLong(USER_ID, userId!!)
                    }
                }
    }
}

package com.kote.martin.wats.model

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Relation


class UserFavPlaces {

    @Embedded
    var user: User? = null

    @Relation(parentColumn = "id", entityColumn = "userId", entity = Place::class)
    var places: List<Place>? = null


    /* Alternatively you can use projection to fetch a specific column (i.e. only name of the pets) from related Pet table. You can uncomment and try below;

   @Relation(parentColumn = "id", entityColumn = "userId", entity = Pet.class, projection = "name")
   public List<String> pets;
   */
}
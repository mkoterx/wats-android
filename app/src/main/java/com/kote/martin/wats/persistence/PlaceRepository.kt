package com.kote.martin.wats.persistence

import android.arch.persistence.room.*
import com.kote.martin.wats.model.Place

@Dao
interface PlaceRepository {

    @get:Query("SELECT * FROM place")
    val all: List<Place>

    @Query("SELECT * FROM place WHERE id = :id")
    fun findById(id: Long): Place

    @Insert
    fun insert(place: Place)

    @Insert
    fun insertAll(vararg places: List<Place>)

    @Update
    fun update(place: Place)

    @Delete
    fun delete(place: Place)
}
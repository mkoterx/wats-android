package com.kote.martin.wats.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.kote.martin.wats.FavPlacesFragment
import com.kote.martin.wats.R
import com.kote.martin.wats.model.Place

class GeneralActivity : AppCompatActivity(), FavPlacesFragment.OnListFragmentInteractionListener {

    private val PLACE = "place"
    private val USER_ID = "userId"
    private val FRAGMENT_FAV_PLACE = "Fragment Favourite Places"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_general)

        val userId = intent.getLongExtra(USER_ID, 0)

        val fragmentManager = supportFragmentManager
        val transaction = fragmentManager.beginTransaction()
        val fragment = FavPlacesFragment()
        val bundle = Bundle()
        bundle.putLong(USER_ID, userId)
        fragment.arguments = bundle
        transaction.add (R.id.fragment_container_general_activity, fragment, FRAGMENT_FAV_PLACE)
        transaction.commit()
    }

    override fun onListFragmentInteraction(item: Place?) {
        val intent = Intent(this, PlaceActivity::class.java).apply {
            putExtra(PLACE, item)
        }
        startActivity(intent)
    }
}

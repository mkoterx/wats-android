package com.kote.martin.wats.persistence

import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.Database
import android.content.Context
import com.kote.martin.wats.R
import com.kote.martin.wats.model.Place
import com.kote.martin.wats.model.User


@Database(entities = [(Place::class), (User::class)], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun userRepo(): UserRepository
    abstract fun placeRepo(): PlaceRepository

    companion object {

        private var INSTANCE: AppDatabase? = null

        // Singleton pattern
        fun getAppDatabase(context: Context): AppDatabase {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        AppDatabase::class.java,
                        context.getString(R.string.database_name))
                        .build()
            }
            return INSTANCE as AppDatabase
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}
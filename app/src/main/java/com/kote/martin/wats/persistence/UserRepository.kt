package com.kote.martin.wats.persistence

import android.arch.persistence.room.*
import com.kote.martin.wats.model.User
import com.kote.martin.wats.model.UserFavPlaces

@Dao
interface UserRepository {

    @get:Query("SELECT * FROM user")
    val all: List<User>

    @Query("SELECT * FROM User WHERE id = :id")
    fun findUserWithFavPlaces(id: Long): UserFavPlaces

    @Query("SELECT * FROM user WHERE id = :id")
    fun findById(id: Long): User

    @Insert
    fun insert(user: User)

    @Insert
    fun insertAll(vararg users: User)

    @Update
    fun update(user: User)

    @Delete
    fun delete(user: User)
}
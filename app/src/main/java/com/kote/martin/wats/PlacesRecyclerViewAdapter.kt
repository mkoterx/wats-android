package com.kote.martin.wats

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView


import com.kote.martin.wats.FavPlacesFragment.OnListFragmentInteractionListener
import com.kote.martin.wats.model.Place
import kotlinx.android.synthetic.main.list_item_place.view.*
import java.util.*


/**
 * [RecyclerView.Adapter] that can display a [Place] and makes a call to the
 * specified [OnListFragmentInteractionListener].
 * TODO: Replace the implementation with code for your data type.
 */
class PlacesRecyclerViewAdapter(
        private val mListener: OnListFragmentInteractionListener?)
    : RecyclerView.Adapter<PlacesRecyclerViewAdapter.ViewHolder>() {

    private lateinit var data: List<Place>

    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as Place
            // Notify the active callbacks interface (the activity, if the fragment is attached to
            // one) that an item has been selected.
            mListener?.onListFragmentInteraction(item)
            data = Collections.emptyList()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item_place, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = data[position]
        holder.mIdView.text = item.id.toString()
        holder.mNameView.text = item.name
        holder.mGmapsIdView.text = item.gmapsId

        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = data.size

    fun setData(data: List<Place>) {
        this.data = data
        notifyDataSetChanged()
    }

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mIdView: TextView = mView.place_id
        val mNameView: TextView = mView.place_name
        val mGmapsIdView: TextView = mView.place_gmaps_id

        override fun toString(): String {
            return super.toString() + " '" + mNameView.text + "'"
        }
    }
}
